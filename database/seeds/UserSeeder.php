<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'aayushibhimani@gmailcom')->get()->first();
        if(!$user)
        {
            User::create([
                'name'=>'Aayushi Bhimani',
                'email'=>'aayushibhimani@gmail.com',
                'password'=>Hash::make('abcd1234'),
                'role'=>'admin'
            ]);
        }else{
            $user->update(['role'=>'admin']);
        }

        User::create([
            'name'=>'John Doe',
            'email'=>'johndoe@gmail.com',
            'password'=>Hash::make('abcd1234'),
        ]);

        User::create([
            'name'=>'Jimmy Doe',
            'email'=>'jimmydoe@gmail.com',
            'password'=>Hash::make('abcd1234'),
        ]);
    }
}
